# New User Page
***
### Introduction
This page have Header and 2 different containers. First one is the "Header" and it has buttons to control containers which are standing side by side under the header with same width x height.The left container under the header is "User Information Table" and it will contain the details of users. The right container under the header is "New User Form" and it has text description with input fields. 
***
#### Header 
The header will be at the top without any margin from top, left and right. Its background color will be `#E2E2E2` (gray). It will contain 4 different elements:
1. New User Button (Refer to [Material Design](https://material.io/develop/web) Buttons)
    + Background-color: `#0C59F3` (Blue)
    + icon: plus-icon
    + icon-color: `#FFFFFF` (White)
    + text: "New User"
    + text-color: `#FFFFFF` (White)
    + margin-left: `15px`
2. Checkbox
    + label: "Hide Disabled User"
    + text-color: `#000000` (Black)
    + color:before: `#FFFFFF` (White)
	+ color:after: `#0C59F3` (Blue)
	+ checkmark: `#FFFFFF` (White)
	+ margin-left: `15px`
3. Save User Button (Refer to [Material Design](https://material.io/develop/web) Buttons)
	+ Background-color: `#0C59F3` (Blue)
    + text: "Save User"
    + text-color: `#FFFFFF` (White)
    + margin-right: `15px`<br>
The height of the header part will be the wrap-content.There will be `10px` margin from top and bottom to increase usability and legibility. 
***
#### User Information Table
In this part user of this page will see the saved user information as a table. This table will have 4 different columns. The column names are as follows respectively "ID", "User Name", "Email" and "Enabled". Constraints for this container like in below:
+ margin-top: `15px`
+ margin-left: `5px` 
The features of the first row is specified in below:
+ background-color: `#0C59F3` (Blue)
+ text-color: `#FFFFFF` (White)

All columns has two different icons. The first icon is arrow drop up and down for ranking and the second one is filter icon. The icons can be found in [material icons](https://material.io/resources/icons/?style=baseline) site. 

The rows for each user details, *known as records in database*, has different backgrounds one by one. First record has white background and the other row's background is light-blue (`#81CCFF`). And for other records background colors should continue with a white-blue-white-blue pattern.
***
#### New User Form
In this container there are several text boxes with explaining label in front of them. These are the user's information that will be registered. The constraints of container is in below: 
+ margin-top: `15px`
+ margin-right: `5px` 

At the top of form there will be a header. The features of header is like that:
+ background-color: `#E2E2E2` (Gray)
+ text-color: `#000000` (Black)
+ text: "New User"

After specifying header of form container the edittext form elements needs to be designed one under the other.
1. Username
	+ text: "Username: "
	+ text-color: `#000000` (black)
	+ margin-top: `15px`
	+ placeholder: blank
2. Display Name
	+ text: "Display Name: "
	+ text-color: `#000000` (black)
	+ margin-top: `15px`
	+ placeholder: blank
3. Phone
	+ text: "Phone: "
	+ text-color: `#000000` (black)
	+ margin-top: `15px`
	+ placeholder: blank
4. Email
	+ text: "Email: "
	+ text-color: `#000000` (black)
	+ margin-top: `15px`
	+ placeholder: blank
5. User Roles
	+ text: "User Roles: "
	+ text-color: `#000000` (black)
	+ margin-top: `15px`
	+ placeholder: "Select user roles... "

	There will be drop-down menu for this input click. The options for drop-down menu will be : "Guest","Admin" and "SuperAdmin".

6. Enabled
	+ checkbox(same with header checkbox)
***
All definitions for elements explanied in above. The functionality will be specified in the functionality and error section.
### Functionality and Error Check
When a user open this page only the "New User" button and checkbox at header container and User Information Table has to be enable. And "save user" button's background has to be %50 transparent.

After "new user" button has clicked then hidden buttons become visible to fill the form containers element to save new user. 

If any of textboxes is empty or not written in proper way then a red warning error appears with respectively to error. If all required input fields are filled with valid credentials and save button has clicked "New User Added" message will appear then information table needs to be refreshed and new user can be seen in there. After that the state of buttons turn to be start position. So just "new user" button will be enabled and others hidden again. 


----------------------------------------------------------------


# Behavior of Components
***
There are 2 scenarios while user wants to view this page. 
1. There is no problem on network
2. There is a problem with network whether server problem or loss of internet connection.

* If there is a problem with network then user will just see an error message on center of page like **"There is a problem on network, please try again later!"**
* If the first scenario occurs then page will load normally. 
***
When user enters the page user can only be able to click "new user" button. **New User Form **'s all elements will be hidden until user clicks "new user" button on Header. 
* If user clicks then a snackbar message will appear as **"Please click 'new user' button at the top."** 
* If there is at least one record in database then it will be shown in **User Information Table**. But if there is no record then there will be a message in this container like **"There is no record saved."** or if the 'Hide Disabled User' button clicked and database has 'Disable' users then there will be a message in this container like **"There is no enable user."**.

#### Header Component's Behavior
***
At first only the "new user" button and "Hide Disabled User" checkbox will be active but "save user" button won't be clickable and it will be 50% transparent.
* If user of the page clicks "new user" button then a **New User Form** becomes visible to fill the form.
* When "Hide Disabled User" checkbox is clicked then **User Information Table** will be refreshed and disabled user will appear in the container.
* "Save user" button is not active until new user form is filled appropriately. If form is filled with valid credentials then this button will be active with %0 transparency.

#### New User Form
***
As it is described above if user not clicked "new user" button then this container won't be visible and in the form area "Please,Click on the top left new user button to add a new user" message needs to display. If user clicks that button then this container's all element becomes visible and fillable.
* The user can edit the areas "username" and "display name" only with alphabetic characters, not numerics.
* The phone area just can be numeric characters and there has to be 10 numbers according to Turkish phone numbers.
* There has to be `'@'` sign in the email and `'.com'`,`'.net'`,`'.org'`,`'.biz'`,`'.info'`,`'.online'` at the end of the email.
* User has to select one of the drop-down menu elements.
* Enabled checkbox is optional to click. It effects the **User Details** table and if user clicks **Hide Disabled User** then all users will be seen at the table.

***NOTE:*** If there is an empty textbox or not selected menu element in this part then that part's border will be red and there will be an error message on that textbox.
If all elements are filled in the valid credentials then user can click "save user" button at the top right. The form data will be loaded into database.
After clicked "save user" button then all container disables again.

#### User Information Table
***
User just can click two different icon in this part: Drop-up arrows and filter icon.The user will only have read access to table. 
* When user clicks arrow then the table will be ordered ascending or descending.
* When user clicks filter then a small search box will open and user should be able to search according to the icon next to the feature so the queries should change for each icon.
* When user not clicked "Hide Disabled User" then all users will be shown in here.
* If there is a problem on database server then an error message will appear on this part as "No connection to Database!".
